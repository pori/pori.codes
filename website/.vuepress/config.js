module.exports = {
  title: "Alex Hernandez | Software Developer",
  description: "I write software for the web and mobile phones.",
  head: [
    [
      "meta",
      { property: "og:image", content: "https://pori.design/banner.png" }
    ],
    ["link", { type: "image/x-icon", rel: "icon", href: "/favicon.ico" }],
    [
      "link",
      {
        type: "text/css",
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Libre+Baskerville|Libre+Franklin"
      }
    ]
  ],
  ga: "UA-77174521-4",
  markdown: {
    anchor: {
      permalink: false
    }
  }
};
